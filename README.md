# install tiago container
### 1 Requirements
3GB absolute minimum storage (I recommend at least 10GB)
some form of Linux (MacOS and Windows may be supported using Vagrant to run Singularity)
### 2 Installation
Download tiago.sif from https://gofile.io/?c=BdWv7y

Install sylabs singularity. Check guides: https://sylabs.io/guides/3.0/user-guide/quick_start.html

Clone tiago-lib from the sensible-robots gitlab group. https://gitlab.com/sc17lw/tiago-lib.git

Move the downloaded .sif file into your cloned tiago-lib folder.

Change the last line of run.sh to ”tiago.sif” in tiago-lib
### 3 Shell into the container
go to tiago-lib in terminal and run the bash script:  
./run.sh for user-space  
sif containers are read-only, so you will need to rebuild tiago.sif if root access is required for things like installing software. (catkin workspaces still have user write access in the .sif)
### 4 Writable container (root access)
 singularity build --sandbox tiago-rw tiago.sif  
Change the last line of run.sh to ”tiago-rw” in tiago-lib  
Change the last line of write.sh to ”tiago-rw” in tiago-lib, delete the line --bind=build_files/pkgs:/pkgs \  
./run.sh for user-space (catkin workspaces are writable)  
./write.sh to install software that require root access
***
# Setup necessary libraries

    apt-get install bison
    apt-get install re2c
    apt-get install scons
***
# Download and build gringo.so python module
You need to build the gringo python module (4.5.4) in the container  
download from : https://sourceforge.net/projects/potassco/files/gringo/4.5.4/, choose gringo-4.5.4-source.tar.gz.   
Note that this must be placed under the container's home directory.
Unpack the archive, cd into the unpacked folder (gringo-4.5.4-source), and do the following:   

    scons configure --build-dir=release      

You will then need to to configure the build/release.py file. modify the following variables in build/release.py:   

    CPPPATH = ['/usr/include/python2.7']    
    WITH_PYTHON = 'python2.7'    
    WITH_TBB = 'tbb'    
Now run:  

    scons --build-dir=release pyclingo   


***

# Build project
Clone this repository into src directory  

    cd ..
    catkin_make
    
***

# Run
please run the following commands in different terminals:  

    roslaunch rosoclingo_gazebo mailbot_scenario.launch

This sets up and starts the gazebo simulator.

    rosrun rosoclingo run-1.0.py --file [PATH TO rosoclingo_aspprograms]/mailbot/instances/graph_wg.lp --file [PATH TO rosoclingo_aspprograms]/mailbot/mailbot.lp  
This terminal runs the ROSoClingo actionlib server with an ASP encoding of the problem and an instance file describing the Willow Gerage environment.

    roslaunch rosoclingo_interfaces mailbot_interfaces.launch  
This terminal runs the interface layer, transforming actions found in the task plan into actionlib requests. Note that while go actions are simulated by gazebo, pickup and deliver actions are abstracted into just succeeding.

    rosrun rosoclingo_client mailbot.py  
This terminal runs a ROSoClingo actionlib client. To issure requests to the mailbot, please specify the origin, the destination and the object. E.g. to deliver a box of chocolate from o9 to o14 type o9 o14 box_of_chocolate.

    rosrun rosoclingo_interfaces unblock.py
declare paths as unblocked. E.g. to declare the path between c8 c9 is unblocked type c8 c9.

    rosrun rosoclingo_interfaces block.py
declare paths as blocked. E.g. to declare the path between c8 c9 is blocked type c8 c9.

    
