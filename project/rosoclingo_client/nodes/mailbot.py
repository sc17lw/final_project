#! /usr/bin/env python

import roslib; roslib.load_manifest('rosoclingo_client')
from rosoclingo.msg import *
import rospy
import actionlib

client = actionlib.ActionClient(rospy.resolve_name("rosoclingo"),ROSoClingoAction)

def send_goal(source,target,object):
 request = "bring(" + str(source) + "," + str(target) + ")"
 goal = ROSoClingoGoal(request,[object])
 print "send " + str(request) + " request"
 return client.send_goal(goal)

def example():
    client.wait_for_server()
    handlers = []
    question = 'Please enter a request \'FROM TO OBJECT\' or cancel a request \'ID\': '
    input = raw_input(question)
    while (input != ""):
        inlist = input.split(" ")
        if len(inlist) == 1:
            handlers[int(inlist[0])].cancel()
            # (len(handlers)-1).cancel()
            print "cancel request ID: " + str((len(handlers)-1))
        elif len(inlist) == 3:
            handlers.append(send_goal(inlist[0],inlist[1],inlist[2]))
            print "request ID: " + str((len(handlers)-1))
        else:
            print "input ignored"
        input = raw_input(question)

if __name__ == '__main__':
    try:
        rospy.init_node('rosoclingo_example_mailbot')
        example()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
