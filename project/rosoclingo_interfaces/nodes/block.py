#! /usr/bin/env python

import roslib; roslib.load_manifest('rosoclingo_interfaces')
from rosoclingo.msg import *
import rospy
import actionlib
from rosoclingo.msg import ROSoClingoOut, ROSoClingoIn




def send_request():
    pub = rospy.Publisher('/rosoclingo/in', ROSoClingoIn,queue_size=10)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        question = "enter your request:"
        input = raw_input(question)
        inlist=input.split(" ")
        request = ROSoClingoIn()
        request.id= "info"
        request.value = "blocked(" + str(inlist[0]) + "," + str(inlist[1]) + ")"
        rospy.loginfo("send "+"blocked(" + inlist[0] + "," + inlist[1] + ")" + "request")
        pub.publish(request)
        rate.sleep()



if __name__ == '__main__':
    try:
        rospy.init_node('ROSoClingo_block')
        send_request()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
