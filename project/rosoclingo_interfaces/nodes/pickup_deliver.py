#! /usr/bin/env python
#--------------------------------------------------------------------------
# Simulated behaviour to successfully pickup and deliver objects
#--------------------------------------------------------------------------
import re
import rospy
from rosoclingo.msg import ROSoClingoOut, ROSoClingoIn
from rosoclingo.srv import *

topic = rospy.Publisher('/rosoclingo/in', ROSoClingoIn)
get_information = rospy.ServiceProxy(rospy.resolve_name("rosoclingo") + "/get_goal_information", GetGoalInformation)

re_PICKUP = re.compile(r'^\s*pickup\((?P<request>([^\)]+))\)')
re_DELIVER = re.compile(r'^\s*deliver\((?P<request>([^\)]+))\)')

#--------------------------------------------------------------------------
# ROSoClingo Callback
#--------------------------------------------------------------------------
def rosoclingoout_callback(message):
    matched1 = re_PICKUP.match(message.action)
    matched2 = re_DELIVER.match(message.action)
    # Ignore non pickup and deliver actions
    if matched1:
        out_message = ROSoClingoIn()
        out_message.id = message.id
        out_message.value = "success"
        object = get_information(matched1.group("request")).information[0]
        rospy.loginfo("Robot successfully picked up : " + str(object))
        topic.publish(out_message)
    elif matched2:
        out_message = ROSoClingoIn()
        out_message.id = message.id
        out_message.value = "success"
        object = get_information(matched2.group("request")).information[0]
        rospy.loginfo("Robot successfully delivered up : " + str(object))
        topic.publish(out_message)
    else:
        return

#--------------------------------------------------------------------------
# Main
#--------------------------------------------------------------------------
if __name__ == '__main__':
    try:
        rospy.init_node('pickup_delivery')
        rospy.Subscriber("/rosoclingo/out",ROSoClingoOut,rosoclingoout_callback)
        rospy.spin()
    except SystemExit:
        print "System exception"
    except rospy.ROSInterruptException:
        print "ROSInterrupt Exception"
    except:
        print "Unexpected exception: ", sys.exc_info()[0]
        raise
