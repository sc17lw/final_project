#! /usr/bin/env python

import roslib; roslib.load_manifest('rosoclingo_interfaces')
from rosoclingo.msg import *
import rospy
import actionlib
from rosoclingo.msg import ROSoClingoOut, ROSoClingoIn
import std_srvs.srv



def send_request():
    pub = rospy.Publisher('/rosoclingo/in', ROSoClingoIn,queue_size=10)
    # mb_cc_srv_ = rospy.ServiceProxy('/move_base/clear_costmaps', std_srvs.srv.Empty)
    # mb_ac_ = actionlib.SimpleActionClient('move_base', MoveBaseAction)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        question = "enter your request:"
        input = raw_input(question)
        inlist=input.split(" ")
        request = ROSoClingoIn()

        # request.id= "abort"
        # request.value = "failure"
        # pub.publish(request)
        #
        # mb_ac_.cancel_goal()
        # mb_cc_srv_()

        request.id= "info"
        request.value = "unblocked(" + str(inlist[0]) + "," + str(inlist[1]) + ")"
        rospy.loginfo("send "+"unblocked(" + inlist[0] + "," + inlist[1] + ")" + "request")


        pub.publish(request)

        # request.id= "info"
        # request.value = "result(unsat)"
        # pub.publish(request)
        rate.sleep()


if __name__ == '__main__':
    try:
        rospy.init_node('ROSoClingo_unblock')
        send_request()
    except rospy.ROSInterruptException:
        print "program interrupted before completion"
