import rospy
import xml.etree.cElementTree as ET
import gringo

class ROSCommands:

    def __init__(self,rosoclingo):
        self.rosoclingo = rosoclingo
        self.commands = {}
        self.commands["assign_external"] = self.__assign_external
        self.commands["assign_messages"] = self.__assign_messages
        self.commands["commit_actions"] = self.__commit_actions
        self.commands["exit"] = self.__exit
        self.commands["ground"] = self.__ground
        self.commands["idle"] = self.__idle
        self.commands["initialize"] = self.__initialize
        self.commands["publish"] = self.__publish
        self.commands["release_external"] = self.__release_external
        self.commands["release_step"] = self.__release_step
        self.commands["reset"] = self.__reset
        self.commands["solve"] = self.__solve
        self.commands["status_update"] = self.__status_update
        self.executed_actions = []
        self.received_messages = []
        rospy.logdebug("ROSoClingo server " + rospy.get_name() + " known commands: " + str(self.commands))

### clingo commands

    def __add_add(self,commands,name,program):
        add = ET.SubElement(commands,"add", name=str(name))
        ET.SubElement(add,"program").text = str(program)

    def __add_cleanup_domains(self,commands):
        ET.SubElement(commands,"cleanup_domains")

    def __assign_event(self,modi,commands,source,event,step):
        eevent = "event(" + str(source) + "," + str(event) + "," + str(step) + ")"
        if str(modi) == "external":
            self.__add_assign_external(commands,eevent,True)
        elif str(modi) == "ground":
            self.__add_ground(commands,[("event",[str(source),str(event),str(step)])])
            self.__release_external(commands,eevent)

    def __add_assign_external(self,commands,external,truth):
        ET.SubElement(commands,"assign_external", name=str(external), value=str(truth))

    def __add_ground(self,commands,programs):
        ground = ET.SubElement(commands,"ground")
        for (name,parameters) in programs:
            program = ET.SubElement(ground, "program", name=str(name))
            for parameter in parameters:
                ET.SubElement(program, "parameter", value=str(parameter))

    def __add_release_external(self,commands,external):
        ET.SubElement(commands,"release_external", name=str(external))

###

    def __assign_external(self,parameter):
        commands = ET.Element("commands")
        self.__add_assign_external(commands,parameter[0],parameter[1])
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.set_active()

    def __assign_messages(self,parameter):
        commands = ET.Element("commands")
        for message in self.rosoclingo.communication.get_pending_messages():
            self.received_messages.append((str(message.id),str(parameter[0])))
            self.__assign_event("external",commands,str(message.id),str(message.value),str(parameter[0]))
        self.rosoclingo.communication.reset_pending_messages()
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.set_active()

    def __commit_actions(self,parameter):
        commands = ET.Element("commands")
        for action in self.rosoclingo.solver.parser.get_actions(parameter[0]):
            if (str(action.args()[0]),str(parameter[0])) not in self.executed_actions:
                self.rosoclingo.communication.publish(str(action.args()[0]),str(action.args()[1]))
                event = "do(" + str(action.args()[0]) + "," + str(action.args()[1]) + ")"
                self.executed_actions.append((str(action.args()[0]),str(parameter[0])))
                self.__assign_event("external",commands,"commit",str(event),str(parameter[0]))
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.set_active()

    def __exit(self,parameter):
        self.rosoclingo.set_exit()

    def __ground(self,parameter):
        commands = ET.Element("commands")
        self.__add_ground(commands,[("state",[parameter[0]]),("transition",[parameter[0]]),("query",[parameter[0]])])
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.set_active()

    def __idle(self,parameter):
        pass

    def __initialize(self,parameter):
        commands = ET.Element("commands")
        self.__add_ground(commands,[("base",[]),("state",[0]),("query",[0])])
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.solver.send_goal_and_wait(ET.Element("assumptions"))
        self.rosoclingo.communication.start()
        self.rosoclingo.set_active()

    def __publish(self,parameter):
        self.rosoclingo.communication.publish("solver",str(self.rosoclingo.solver.parser.get_answerset()))

    def __release_external(self,parameter):
        commands = ET.Element("commands")
        self.__add_release_external(commands,parameter[0])
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.set_active()

    def __release_step(self,parameter):
        commands = ET.Element("commands")
        ET.SubElement(commands,"get_externals")
        result = self.rosoclingo.solver.control(commands)
        externals = ET.fromstring(result.result)[0]
        events = self.rosoclingo.solver.parser.get_events(parameter[0])
        commands = ET.Element("commands")
        for external in externals:
            atom = gringo.parse_term(external.text)
            if atom.name() == "event" and len(atom.args()) == 3 and atom.args()[2] == int(parameter[0]):
                if atom.args() not in [event.args() for event in events]:
                    self.__add_release_external(commands,str(atom))
        self.__add_cleanup_domains(commands)
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.set_active()

    def __solve(self,parameter):
        self.rosoclingo.solver.send_goal(ET.Element("assumptions"),self.on_solve_return)

    def __status_update(self,parameter):
        status = self.rosoclingo.solver.parser.get_status(parameter[0])
        self.rosoclingo.communication.goal_handler.status_update(status)
        self.rosoclingo.set_active()

    def __reset(self,parameter):
        commands = ET.Element("commands")
        for event in self.rosoclingo.solver.parser.get_all_events():
            self.__add_assign_external(commands,event,False)
        for fluent in self.rosoclingo.solver.parser.get_state(parameter[0]):
            event = "event(save," + str(fluent.args()[0]) + "," + str(0) + ")"
            self.__add_assign_external(commands,event,True)
        self.rosoclingo.communication.goal_handler.request_cleanup()
        self.rosoclingo.solver.control(commands)
        self.rosoclingo.communication.goal_handler.set_reset(False)
        self.executed_actions = []
        self.received_messages = []
        self.rosoclingo.set_active()

#####

    def on_solve_return(self,state,result):
        rospy.logdebug("ROSoClingo server " + rospy.get_name() + " got a result form \solver!")
        if state == 3:
            self.rosoclingo.solver.lock.acquire()
            self.rosoclingo.solver.parser.set_goal_return(result)
            self.rosoclingo.solver.lock.release()
            self.rosoclingo.set_active()

    def get_controller_info(self,step):
        events = []
        if not set(self.received_messages).issuperset(set(self.executed_actions)):
            events.append("event(info,pending_actions," + str(step) + ")")
        return events

#####

    def execute(self,action,parameter):
        rospy.logdebug("ROSoClingo server " + rospy.get_name() + " executing command " + action + " " + str(parameter) + "!")
        self.commands[action](parameter)
