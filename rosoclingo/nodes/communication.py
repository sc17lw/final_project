import rospy
import actionlib
from rosoclingo.msg import *
from rosclingo.msg import *
from rosclingo.srv import *
import xml.etree.cElementTree as ET
from parser import ROSASPParser
from threading import Lock

class ROSClingoController:

    def __init__(self,name):
        self.lock = Lock()
        self.client = actionlib.SimpleActionClient(name, ROSClingoAction)
        self.service = rospy.ServiceProxy(name+"/control",ROSClingoControl,persistent=True)
        self.parser = ROSASPParser()
        rospy.loginfo("ROSoClingo server " + rospy.get_name() + " wait for ROSClingo actionlib server: "+name)
        self.client.wait_for_server()

    def control(self,commands):
        return self.service(ET.tostring(commands))

    def send_goal_and_wait(self,assumptions=ET.Element("assumptions")):
        state = self.client.send_goal_and_wait(ROSClingoGoal(ET.tostring(assumptions)))
        self.parser.set_goal_return(self.client.get_result())
        return state

    def send_goal(self,assumptions=ET.Element("assumptions"),callback=None):
        self.client.send_goal(ROSClingoGoal(ET.tostring(assumptions)),done_cb = callback)

class SimpleROSGoalHandler:

    def __init__(self,rosoclingo):
        self.active = {}
        self.pending = []
        self.reset = False
        self.rosoclingo = rosoclingo

    def __get_key(self,request):
        if self.active is None: return None
        for key in self.active.keys():
            if type(self.active[key]) == type(request):
                if self.active[key] == request:
                    return key
        return None

    def add_request_keys(self,request_keys):
        for id in request_keys: self.active[str(id)] = "free"        

    def add_request(self,request):
        key = self.__get_key("free")
        if key is not None:
            self.active[key] = request
        else:
            self.pending.append(request)
            return None
        return key

    def status_update(self,update):
        for atom in update:
            key = str(atom.args()[0])
            status = str(atom.args()[1])
            if type(self.active[key]) != type(""):
                currentStatus = self.active[key].get_goal_status().status
                if status == "accepted" and currentStatus in [0, 7]:
                    self.active[key].set_accepted()
                elif status == "rejected" and currentStatus in [0, 7]:
                    self.active[key].set_rejected()
                    self.set_reset(True)
                elif status == "succeeded" and currentStatus in [1, 6]:
                    self.active[key].set_succeeded()
                    self.set_reset(True)
                elif status == "canceled" and currentStatus in [7, 6]:
                    self.active[key].set_canceled()
                    self.set_reset(True)
                elif status == "aborted" and currentStatus in [1, 6]:
                    self.active[key].set_aborted()
                    self.set_reset(True)

    def cancel_request(self,request):
        key = self.__get_key(request)
        if key is None:
            self.pending.remove(request)
            request.set_canceled()
        return key

    def request_cleanup(self):
        for key in self.active.keys():
            if type(self.active[key]) is not str:
                if self.active[key].get_goal_status().status in [2, 3, 4, 5, 8]:
                    self.active[key] = "free"
        while(self.pending != []):
            key = self.__get_key("free")
            if key is not None:
                self.active[key] = self.pending.pop()
                message = ROSoClingoIn(key,self.active[key].goal.goal.request)
                self.reset = True
                self.rosoclingo.communication.pending_messages.append(message)
            else: break

    def get_reset(self):
        return self.reset

    def set_reset(self,reset):
        self.reset = reset

class ROSCommunication:

    def __init__(self,rosoclingo):
        self.rosoclingo = rosoclingo
        self.controller = ROSClingoController("controller")
        self.solver = ROSClingoController("planner")
        self.goal_handler = SimpleROSGoalHandler(rosoclingo)
        self.send_messages = []
        self.pending_messages = []
        self.started = False
        self.publisher = rospy.Publisher(rospy.get_name()+'/out',ROSoClingoOut,queue_size=10,latch=True)

    def start(self):
        if not self.started:
            self.started = True
            self.goal_handler.add_request_keys(self.rosoclingo.solver.parser.get_request_ids())
            rospy.Subscriber(rospy.get_name()+"/in",ROSoClingoIn,self.__receiving)
            rospy.loginfo("ROSoClingo server " + rospy.get_name() + " is ready to accept goals")
            actionlib.ActionServer(rospy.get_name(),ROSoClingoAction,self.__requesting,self.__canceling,auto_start=False).start()

    def __receiving(self,message):
        self.pending_messages.append(message)
        self.rosoclingo.set_active()

    def __requesting(self,request):
        key = self.goal_handler.add_request(request)
        if key is not None:
            message = ROSoClingoIn(key,request.goal.goal.request)
            self.pending_messages.append(message)
            self.rosoclingo.set_active()

    def __canceling(self,request):
        key = self.goal_handler.cancel_request(request)
        if key is not None:
            message = ROSoClingoIn(id,"cancel")
            self.pending_messages.append(message)
            self.rosoclingo.set_active()

    def get_controller(self):
        return self.controller

    def get_solver(self):
        return self.solver

    def get_pending_messages(self):
        return self.pending_messages

    def publish(self,source,content):
        message = ROSoClingoOut(source,content)
        self.publisher.publish(message)
        rospy.sleep(0.1)

    def reset_pending_messages(self):
        self.pending_messages = []

    def get_controller_info(self,step):
        events = []
        if self.pending_messages != []: events.append("event(info,new_messages," + str(step) + ")")
        if self.goal_handler.get_reset(): events.append("event(info,request_reset," + str(step) + ")")
        return events
