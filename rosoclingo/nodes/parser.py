import gringo
import rospy

class ROSASPParser:

    def __init__(self):
        self.result = None
        self.interrupted = False
        self.answerset = []
        self.__reset()

    def set_goal_return(self,goal):
        if goal is not None:
            self.__reset()
            self.result = goal.result
            self.interrupted = goal.interrupted
            self.answerset = map(gringo.parse_term,goal.answerset)
            self.__parse()
        else:
            rospy.logerr("ROSoClingo server " + rospy.get_name() + " parser.set_goal_return goal parameter is None!")

    def __reset(self):
        self.actions = {}
        self.state = {}
        self.events = {}
        self.status = {}

    def __parse(self):
        for atom in self.answerset:
            if atom.name() == "do" and len(atom.args()) == 3:
                key = atom.args()[2]
                if key in self.actions.keys(): self.actions[key].append(atom)
                else: self.actions[key] = [atom]
            if atom.name() == "holds" and len(atom.args()) == 2:
                key = atom.args()[1]
                if key in self.state.keys(): self.state[key].append(atom)
                else: self.state[key] = [atom]
            if atom.name() == "event" and len(atom.args()) == 3:
                key = atom.args()[2]
                if key in self.events.keys(): self.events[key].append(atom)
                else: self.events[key] = [atom]
            if atom.name() == "status" and len(atom.args()) == 3:
                key = atom.args()[2]
                if key in self.status.keys(): self.status[key].append(atom)
                else: self.status[key] = [atom]

    def get_answerset(self):
        return self.answerset

    def get_result(self):
        return self.result

    def get_request_ids(self):
        ids = []
        for atom in self.answerset:
            if atom.name() == "rslot" and len(atom.args()) == 1:
                ids.append(atom.args()[0])
        return ids

    def get_atoms(self,name,arguments):
        atoms = []
        for atom in self.answerset:
            if atom.name() == name and len(atom.args()) == arguments:
                atoms.append(atom)
        return atoms

    def get_all_events(self):
        result = []
        for l in self.events.items(): result += l[1]
        return result

    def get_events(self,key):
        key = gringo.parse_term(str(key))
        if key in self.events.keys():
            return self.events[key]
        else:
            return []

    def get_state(self,key):
        key = gringo.parse_term(str(key))
        if key in self.state.keys():
            return self.state[key]
        else:
            rospy.logdebug("ROSoClingo server " + rospy.get_name() + " parser.get_state got an unknown key! [] is returned")
            return []

    def get_status(self,key):
        key = gringo.parse_term(str(key))
        if key in self.status.keys():
            return self.status[key]
        else:
            rospy.logdebug("ROSoClingo server " + rospy.get_name() + " parser.get_state got an unknown key! [] is returned")
            return []

    def get_actions(self,key):
        key = gringo.parse_term(str(key))
        if key in self.actions.keys():
            return self.actions[key]
        else:
            rospy.logdebug("ROSoClingo server " + rospy.get_name() + " parser.get_actions got an unknown key! [] is returned")
            return []
