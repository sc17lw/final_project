#! /usr/bin/env python
import roslib
import rospy
roslib.load_manifest('rosoclingo')

import xml.etree.cElementTree as ET

from communication import ROSCommunication
from commands import ROSCommands

from threading import Event, Lock
from threading import Thread

##### ROSoClingo

class ROSoClingo:

    ACTIVE   = 1
    IDLE     = 2
    EXIT     = 3

    def __init__(self):
        self.interrupt = Event()
        self.lock = Lock()
        self.old_events = []

        self.communication = ROSCommunication(self)
        self.controller = self.communication.get_controller()
        self.solver = self.communication.get_solver()
        self.commands = ROSCommands(self)

        commands = ET.Element("commands")
        ground = ET.SubElement(commands,"ground")
        ET.SubElement(ground, "program", name="base")
        self.controller.control(commands)
        self.state = ROSoClingo.ACTIVE
        self.thread = Thread(target=self.__run, args=())
        self.thread.start()

    def __run(self):
        while True:
            self.interrupt.clear()
            if self.state == ROSoClingo.IDLE:
                rospy.logdebug("ROSoClingo server " + rospy.get_name() + " enters IDLE state")
                self.interrupt.wait()
            elif self.state == ROSoClingo.ACTIVE:
                self.lock.acquire()
                self.state = ROSoClingo.IDLE
                rospy.logdebug("ROSoClingo server " + rospy.get_name() + " enters ACTIVE state")
                self.controller_update()
                self.controller.send_goal_and_wait(ET.Element("assumptions"))
                action_list = self.controller.parser.get_actions("1")
                if action_list == []:
                    rospy.logerr("ROSoClingo server " + rospy.get_name() + " get_solver_commands function expects actions! No action is executed!")
                for action in [atom.args()[1]for atom in action_list]:
                    self.commands.execute(str(action.name()),action.args())
                self.lock.release()
            elif self.state == ROSoClingo.EXIT:
                rospy.signal_shutdown("By demand of controller")
                return

    def set_active(self):
        if self.state != ROSoClingo.EXIT:
            self.state = ROSoClingo.ACTIVE
        self.interrupt.set()

    def set_exit(self):
        self.state = ROSoClingo.EXIT

    def controller_update(self):
        events = []
        result = self.solver.parser.get_result()
        if result is not None: events.append("event(info,result("+str(result)+"),0)")
        for fluent in self.controller.parser.get_state(1): events.append("event(save,"+str(fluent.args()[0].args()[0])+",0)")
        events += self.commands.get_controller_info(0)
        events += self.communication.get_controller_info(0)
        commands = ET.Element("commands")
        for event in set(self.old_events)-set(events): ET.SubElement(commands,"assign_external", name=event, value="false")
        for event in set(events)-set(self.old_events): ET.SubElement(commands,"assign_external", name=event, value="true")
        self.controller.control(commands)
        self.old_events = events

    def exit(self):
        self.state = ROSoClingo.EXIT
        self.interrupt.set()
        self.thread.join()
        rospy.signal_shutdown("By demand of exit_function")
        return None

if __name__ == '__main__':
    rospy.init_node('rosoclingo')
    rosoclingo = ROSoClingo()
    rospy.spin()
    rosoclingo.exit()
